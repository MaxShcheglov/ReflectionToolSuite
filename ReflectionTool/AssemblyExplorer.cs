﻿using System.Collections.Generic;
using System.Text;

namespace ReflectionTool
{
	public class AssemblyExplorer
	{
		private List<ITypeExplorer> explorers = new List<ITypeExplorer>();
		private StringBuilder builder = new StringBuilder();

		public AssemblyExplorer()
		{

		}

		public AssemblyExplorer(List<ITypeExplorer> explorers)
		{
			this.explorers.AddRange(explorers);
		}

		public string Explore(IAssembly assembly)
		{
			builder.AppendLine("Assembly name: " + assembly.Name);
			
			foreach(var type in assembly.Types)
			{
				builder.AppendLine("\tType name: " + type.Name);

				foreach(var explorer in explorers)
				{
					builder.AppendLine($"\t\t{explorer.Prefix}: ");
					builder.AppendLine($"\t\t\t{string.Join("\n", explorer.Explore(type))}");
				}
			}

			return builder.ToString();
		}
	}
}
