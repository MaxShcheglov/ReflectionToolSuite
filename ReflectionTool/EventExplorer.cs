﻿using System;
using System.Linq;

namespace ReflectionTool
{
	public class EventExplorer : ITypeExplorer
	{
		public string Prefix => "Type events";

		public string[] Explore(Type type)
		{
			return type.GetEvents().Select((t) => (t.ToString())).ToArray();
		}
	}
}
