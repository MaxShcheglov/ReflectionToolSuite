﻿using System;
using System.Linq;

namespace ReflectionToolTests
{
	public class FieldExplorer
	{
		public string[] Explore(Type type)
		{
			return type.GetFields().Select((t) => (t.ToString())).ToArray();
		}
	}
}