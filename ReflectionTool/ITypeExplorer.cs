﻿using System;

namespace ReflectionTool
{
	public interface ITypeExplorer
	{
		string Prefix { get; }
		string[] Explore(Type type);
	}
}
