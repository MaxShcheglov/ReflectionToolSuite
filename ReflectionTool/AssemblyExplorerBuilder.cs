﻿using System.Collections.Generic;

namespace ReflectionTool
{
	public class AssemblyExplorerBuilder
	{
		private List<ITypeExplorer> explorers = new List<ITypeExplorer>();

		public AssemblyExplorerBuilder WithExplorer(ITypeExplorer explorer)
		{
			AddExplorerIfNeeded(explorer);

			return this;
		}

		private void AddExplorerIfNeeded(ITypeExplorer explorer)
		{
			if (!IsSameExplorerAlreadyExist(explorer))
				explorers.Add(explorer);
		}

		private bool IsSameExplorerAlreadyExist(ITypeExplorer explorer)
		{
			return explorers.Exists((e) => e.ToString() == explorer.ToString());
		}

		public AssemblyExplorer Build()
		{
			return new AssemblyExplorer(explorers);
		}
	}
}
