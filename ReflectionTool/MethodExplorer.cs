﻿using System;
using System.Linq;

namespace ReflectionTool
{
	public class MethodExplorer : ITypeExplorer
    {
		public string Prefix => "Type methods";

		private string[] exceptedMethods = new string[] {   "System.String ToString()",
															"Boolean Equals(System.Object)",
															"Int32 GetHashCode()",
															"System.Type GetType()" };

		public string[] Explore(Type type)
		{
			return type.GetMethods().Select((t) => (t.ToString())).Except(exceptedMethods).ToArray();
		}
    }
}
