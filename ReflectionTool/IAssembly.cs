﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionTool
{
	public interface IAssembly
	{
		string Name { get; }
		Type[] Types { get; }
	}
}
