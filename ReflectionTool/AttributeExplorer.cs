﻿using System;

namespace ReflectionTool
{
	public class AttributeExplorer : ITypeExplorer
	{
		public string Prefix => "Type attributes";

		public string[] Explore(Type type)
		{
			return new string[] { type.Attributes.ToString() };
		}
	}
}
