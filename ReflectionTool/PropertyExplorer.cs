﻿using System;
using System.Linq;

namespace ReflectionTool
{
	public class PropertyExplorer : ITypeExplorer
    {
		public string Prefix => "Type properties";

		public string[] Explore(Type type)
		{
			return type.GetProperties().Select((t) => (t.ToString())).ToArray();
		}
    }
}
