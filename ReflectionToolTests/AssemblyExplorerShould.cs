﻿using ReflectionTool;
using NUnit.Framework;
using ReflectionToolTests.Fakes;

namespace ReflectionToolTests
{
	[TestFixture]
	public class AssemblyExplorerShould
	{
		[Test]
		public void ReturnAssemblyAndTypeNamesIfExplorerHasNoTypeExplorers()
		{
			var explorer = new AssemblyExplorer();

			var result = explorer.Explore(new AssemblyWithOneTypeFake());

			var expected = "Assembly name: AssemblyWithOneTypeFake\r\n\t" +
						   "Type name: TypeWithOneMethodFake\r\n";
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnAssemblyNameIfExplorerhasNoTypeExplorersAndPassedAssemblyWithNoOneType()
		{
			var explorer = new AssemblyExplorer();

			var result = explorer.Explore(new AssemblyWithNoOneTypeFake());

			var expected = "Assembly name: AssemblyWithNoOneTypeFake\r\n";
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnAssemblyNameIfExplorerHasTypeExplorerAndPassedAssemblyWithNoOneType()
		{
			var explorer = new AssemblyExplorerBuilder().
				WithExplorer(new MethodExplorer()).
				Build();

			var result = explorer.Explore(new AssemblyWithNoOneTypeFake());

			var expected = "Assembly name: AssemblyWithNoOneTypeFake\r\n";
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnAssemblyAndTypeNamesAndMethodsNameIfExplorerHasTypeExplorerAndPassedAssemblyWithOneType()
		{

			var explorer = new AssemblyExplorerBuilder().
				WithExplorer(new MethodExplorer()).
				Build();

			var result = explorer.Explore(new AssemblyWithOneTypeFake());

			var expected = "Assembly name: AssemblyWithOneTypeFake\r\n\t" +
						   "Type name: TypeWithOneMethodFake\r\n\t\t" +
						   "Type methods: \r\n\t\t\t" +
						   "Void SampleMethod()\r\n";
			Assert.That(expected, Is.EqualTo(result));
		}
	}
}