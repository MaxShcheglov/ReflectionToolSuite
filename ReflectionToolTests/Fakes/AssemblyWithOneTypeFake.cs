﻿using System;
using ReflectionTool;

namespace ReflectionToolTests.Fakes
{
	public class AssemblyWithOneTypeFake : IAssembly
	{
		public string Name => "AssemblyWithOneTypeFake";

		public Type[] Types => new Type[] { typeof(TypeWithOneMethodFake) };
	}
}
