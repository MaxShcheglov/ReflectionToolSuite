﻿using System;
using ReflectionTool;

namespace ReflectionToolTests.Fakes
{
	public class AssemblyWithNoOneTypeFake : IAssembly
	{
		public string Name => "AssemblyWithNoOneTypeFake";

		public Type[] Types => Array.Empty<Type>();
	}
}
