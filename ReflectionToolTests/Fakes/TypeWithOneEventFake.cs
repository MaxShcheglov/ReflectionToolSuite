﻿using System;

namespace ReflectionToolTests.Fakes
{
	class TypeWithOneEventFake
	{
		public event EventHandler SampleEvent;
	}
}
