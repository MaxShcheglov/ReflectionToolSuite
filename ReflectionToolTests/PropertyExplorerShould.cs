﻿using System;
using NUnit.Framework;
using ReflectionTool;
using ReflectionToolTests.Fakes;

namespace ReflectionToolTests
{
	[TestFixture]
    public class PropertyExplorerShould
    {
		[Test]
		public void ReturnArrayOfEventNamesIfPassedTypeWithEvents()
		{
			var explorer = new PropertyExplorer();

			var result = explorer.Explore(typeof(TypeWithOnePropertyFake));

			var expected = new string[]
			{
				"Int32 SampleProperty"
			};
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnEmptyArrayOfEventNamesIfPassedTypeWithNoEvents()
		{
			var explorer = new EventExplorer();

			var result = explorer.Explore(typeof(EmptyTypeFake));

			Assert.That(Array.Empty<string>, Is.EqualTo(result));
		}
    }
}
