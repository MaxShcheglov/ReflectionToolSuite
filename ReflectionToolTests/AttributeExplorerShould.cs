﻿using NUnit.Framework;
using ReflectionTool;
using ReflectionToolTests.Fakes;

namespace ReflectionToolTests
{
	[TestFixture]
    public class AttributeExplorerShould
    {
		[Test]
		public void ReturnArrayOfAttributeNamesIfPassedTypeWithAttributes()
		{
			var explorer = new AttributeExplorer();

			var result = explorer.Explore(typeof(TypeWithOneAttributeFake));

			var expected = new string[]
			{
				"AutoLayout, AnsiClass, Class, Serializable, BeforeFieldInit"
			};
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnArrayOfDefaultAttributeNamesIfPassedTypeWithNoOneAttribute()
		{
			var explorer = new AttributeExplorer();

			var result = explorer.Explore(typeof(EmptyTypeFake));

			var expected = new string[]
			{
				"AutoLayout, AnsiClass, Class, BeforeFieldInit"
			};
			Assert.That(expected, Is.EqualTo(result));
		}
    }
}
