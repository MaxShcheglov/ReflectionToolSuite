﻿using NUnit.Framework;
using ReflectionTool;
using ReflectionToolTests.Fakes;
using System;

namespace ReflectionToolTests
{
	[TestFixture]
    public class MethodExplorerShould
    {
		[Test]
		public void ReturnArrayOfMethodNamesIfPassedTypeWithMethods()
		{
			var explorer = new MethodExplorer();

			var result = explorer.Explore(typeof(TypeWithOneMethodFake));

			var expected = new string[]
			{
					"Void SampleMethod()"
			};
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnArrayOfExtensionMethodsIfPassedTypeWithNoOneMethod()
		{
			var explorer = new MethodExplorer();

			var result = explorer.Explore(typeof(EmptyTypeFake));

			Assert.That(Array.Empty<string>, Is.EqualTo(result));
		}
    }
}
