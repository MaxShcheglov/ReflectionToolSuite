﻿using System;
using NUnit.Framework;
using ReflectionTool;
using ReflectionToolTests.Fakes;

namespace ReflectionToolTests
{
	[TestFixture]
	public class FieldExplorerShould
	{
		[Test]
		public void ReturnArrayOfFieldNamesIfPassedTypeWithFields()
		{
			var explorer = new FieldExplorer();

			var result = explorer.Explore(typeof(TypeWithOneFieldFake));

			var expected = new string[]
			{
				"Int32 SampleField"
			};
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnEmptyArrayOfFieldNamesIfPassedTypeWithNoOneField()
		{
			var explorer = new FieldExplorer();

			var result = explorer.Explore(typeof(EmptyTypeFake));

			Assert.That(Array.Empty<string>, Is.EqualTo(result));
		}
	}
}