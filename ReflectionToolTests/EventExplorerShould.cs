﻿using System;
using NUnit.Framework;
using ReflectionTool;
using ReflectionToolTests.Fakes;

namespace ReflectionToolTests
{
	[TestFixture]
    public class EventEmplorerShould
    {
		[Test]
		public void ReturnArrayOfEventNamesIfPassedTypeWithEvents()
		{
			var explorer = new EventExplorer();

			var result = explorer.Explore(typeof(TypeWithOneEventFake));

			var expected = new string[]
			{
				"System.EventHandler SampleEvent"
			};
			Assert.That(expected, Is.EqualTo(result));
		}

		[Test]
		public void ReturnEmptyArrayOfEventNamesIfPassedTypeWithNoEvents()
		{
			var explorer = new EventExplorer();

			var result = explorer.Explore(typeof(EmptyTypeFake));

			Assert.That(Array.Empty<string>, Is.EqualTo(result));
		}
    }
}
